﻿using Pachyderm.Database.Entities;
using Raven.Abstractions.Commands;
using Raven.Client.Embedded;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pachyderm.Database
{
    public class DatabaseClient
    {
        EmbeddableDocumentStore _documentStore;

        public DatabaseClient() : this(false)
        { }

        public DatabaseClient(bool runInMemory)
        {
            _documentStore = new EmbeddableDocumentStore
            {
                RunInMemory = runInMemory,
                DataDirectory = GetDataDirectory()
            };

            // Initialize or create database if needed
            _documentStore.Initialize();

            // Run all index creation tasks
            Raven.Client.Indexes.IndexCreation.CreateIndexes(this.GetType().Assembly, _documentStore);
        }

        private static string GetDataDirectory()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Pachyderm", "Data");
        }

        public List<T> GetAll<T>() where T : Entity
        {
            using (var session = _documentStore.OpenSession())
            {
                var all = session
                    .Query<T>()
                    .ToList();

                return all;
            }
        }

        public T Load<T>(string id) where T : Entity
        {
            using (var session = _documentStore.OpenSession())
            {
                return session.Load<T>(id);
            }
        }

        public void Store(Entity entity)
        {
            using (var session = _documentStore.OpenSession())
            {
                session.Store(entity);
                session.SaveChanges();
            }
        }

        public void Store(IEnumerable<Entity> entities)
        {
            using (var session = _documentStore.OpenSession())
            {
                foreach (var entity in entities)
                {
                    session.Store(entity);
                }
                session.SaveChanges();
            }
        }

        public void Delete(Entity entity)
        {
            using (var session = _documentStore.OpenSession())
            {
                session.Delete(entity);
                session.SaveChanges();
            }
        }

        public void Delete(IEnumerable<Entity> entities)
        {
            using (var session = _documentStore.OpenSession())
            {
                foreach (var entity in entities)
                {
                    session.Delete(entity);
                }
                session.SaveChanges();
            }
        }

        public void DeleteById(string id)
        {
            using (var session = _documentStore.OpenSession())
            {
                session.Advanced.Defer(new DeleteCommandData { Key = id });
                session.SaveChanges();
            }
        }

        public void DeleteById(IEnumerable<string> ids)
        {
            using (var session = _documentStore.OpenSession())
            {
                foreach (var id in ids)
                {
                    session.Advanced.Defer(new DeleteCommandData { Key = id });
                }
                session.SaveChanges();
            }
        }
    }
}
