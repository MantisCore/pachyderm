﻿using CefSharp;
using CefSharp.Wpf;
using Pachyderm.Services;
using Pachyderm.Presentation.ResourceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pachyderm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(AppCallbackService callbackService)
        {
            InitializeComponent();

            var settings = new CefSettings()
            {
                PackLoadingDisabled = true
            };
            settings.RegisterScheme(new CefCustomScheme()
                {
                    SchemeName = ResourceSchemeHandlerFactory.SchemeName,
                    SchemeHandlerFactory = new ResourceSchemeHandlerFactory()
                });

            if (Cef.Initialize(settings))
            {
                var browser = new ChromiumWebBrowser();
                //browser.ResourceHandlerFactory = new LocalResourceHandlerFactory();
                browser.RegisterJsObject("appCallbackService", callbackService);
                main_grid.Children.Add(browser);

                browser.Address = "resource://presentation/Views/TestPageView.html";
            }
        }
    }
}
