﻿using Autofac;
using Pachyderm.Database;
using Pachyderm.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pachyderm
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();

            // Register all services
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsSelf()
                .SingleInstance();

            builder.Register(c => new DatabaseClient(true)) // true for testing
                .AsSelf()
                .SingleInstance();
        }
    }
}
