﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pachyderm.Presentation.ResourceLocation
{
    [Obsolete]
    public class LocalResourceHandlerFactory : DefaultResourceHandlerFactory
    {
        const string rootFolder = "Presentation";
        const string resourceAddress = "http://resources/";

        public LocalResourceHandlerFactory() : base()
        {

        }

        public override ResourceHandler GetResourceHandler(IWebBrowser browser, IRequest request)
        {
            var stream = GetStream(request.Url);

            if (stream != null)
                return ResourceHandler.FromStream(stream);
            else
                return base.GetResourceHandler(browser, request);

            //// TEMPORARY SOLUTION UNTIL NEXT RELEASE
            //var url = request.Url;
            //if (url.ToLower().StartsWith(resourceAddress) && url.Length > 17)
            //{
            //    var filePath = GetFilePath(url.Substring(17));
            //    return ResourceHandler.FromFileName(filePath);
            //}
            //else
            //    return base.GetResourceHandler(browser, request);
        }

        protected Stream GetStream(string url)
        {
            if (url.ToLower().StartsWith(resourceAddress) && url.Length > 17)
            {
                var name = url.Substring(17);
                var stream = GetResourceStream(GetResourcePath(name));

                if (stream == null)
                    stream = GetFileStream(GetFilePath(name));

                return stream;
            }

            return null;
        }

        protected string GetResourcePath(string name)
        {
            var resourcePath = "/" + rootFolder + "/" + name;
            return resourcePath;
        }

        protected string GetFilePath(string name)
        {
            var filePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase), rootFolder, name).Replace(@"file:\", "").Replace("/", "\\");
            return filePath;
        }

        protected Stream GetResourceStream(string resourceName)
        {
            var uri = new Uri(resourceName, UriKind.Relative);
            var info = System.Windows.Application.GetResourceStream(uri);

            if (info == null)
                return null;

            return info.Stream;
        }

        protected Stream GetFileStream(string filePath)
        {
            var stream = new MemoryStream();

            var file = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            var bytes = new byte[file.Length];
            file.Read(bytes, 0, (int)file.Length);
            stream.Write(bytes, 0, (int)file.Length);
            file.Close();

            return stream;
        }
    }
}
