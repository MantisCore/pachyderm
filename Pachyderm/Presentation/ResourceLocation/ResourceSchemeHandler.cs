﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pachyderm.Presentation.ResourceLocation
{
    public class ResourceSchemeHandler : ISchemeHandler
    {
        public bool ProcessRequestAsync(IRequest request, ISchemeHandlerResponse response, OnRequestCompletedHandler requestCompletedCallback)
        {
            Uri u = new Uri(request.Url);
            String file = u.Authority + u.AbsolutePath;

            Assembly ass = Assembly.GetExecutingAssembly();
            String resourcePath = ass.GetName().Name + "." + file.Replace("/", ".");

            if (ass.GetManifestResourceInfo(resourcePath) != null)
            {
                response.ResponseStream = ass.GetManifestResourceStream(resourcePath);
                response.MimeType = GetMimeType(Path.GetExtension(file));
                requestCompletedCallback();
                return true;
            }

            if (File.Exists(file))
            {
                Byte[] bytes = File.ReadAllBytes(file);
                response.ResponseStream = new MemoryStream(bytes);
                response.MimeType = GetMimeType(Path.GetExtension(file));
                requestCompletedCallback();
                return true;
            }

            return false;
        }

        private string GetMimeType(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".html":
                    return "text/html";
                case ".css":
                    return "text/css";
                case ".js":
                    return "text/javascript";
                case ".jpg":
                    return "image/jpeg";
                case ".gif":
                    return "image/gif";
                case ".png":
                    return "image/png";
                case ".appcache":
                case ".manifest":
                    return "text/cache-manifest";
                default:
                    return "application/octet-stream";
            }
        }
    }
}
