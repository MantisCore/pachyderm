﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pachyderm.Presentation.ResourceLocation
{
    public class ResourceSchemeHandlerFactory : ISchemeHandlerFactory
    {
        public static string SchemeName { get { return "resource"; } }

        public ISchemeHandler Create()
        {
            return new ResourceSchemeHandler();
        }
    }
}