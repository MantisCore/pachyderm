﻿function MainPageViewModel() {
    var self = this;

    // Properties

    self.test = "Hello World!";
    self.testObservable = ko.observable("Observable text");

    self.title = "TITLE";
    self.originalTitle = "ORG TITLE";


    // Functions

    self.AlertOsVersionInfoClicked = function () {
        alert(appCallbackService.getHostOsVersion());
    };

    self.AddBook = function () {
        appCallbackService.addBook(self.title, self.originalTitle);
    };

    self.LoadBooks = function () {
        alert(appCallbackService.loadBooks());
    };
}

// Activates knockout.js
ko.applyBindings(new MainPageViewModel());