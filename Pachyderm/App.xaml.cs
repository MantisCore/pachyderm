﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Pachyderm
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            // Autofac
            var builder = new ContainerBuilder();
            builder.RegisterType<MainWindow>();
            builder.RegisterModule(new AutofacModule());
            var container = builder.Build();

            var mainWindow = container.Resolve<MainWindow>();
            mainWindow.Show();
        }
    }
}
