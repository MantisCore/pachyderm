﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pachyderm.Services
{
    public class AppCallbackService
    {
        private Pachyderm.Database.DatabaseClient _databaseClient;

        public AppCallbackService(Pachyderm.Database.DatabaseClient databaseClient)
        {
            _databaseClient = databaseClient;
        }

        public string getHostOsVersion()
        {
            return Environment.OSVersion.VersionString;
        }

        public void addBook(string title, string originalTitle)
        {
            _databaseClient.Store(new Pachyderm.Database.Entities.BookEntity() { Title = title, OriginalTitle = originalTitle });
        }

        public string loadBooks()
        {
            var books = _databaseClient.GetAll<Pachyderm.Database.Entities.BookEntity>();
            var sb = new StringBuilder();
            foreach (var book in books)
            {
                sb.AppendLine(book.Title + " : " + book.OriginalTitle);
            }

            return sb.ToString();
        }
    }
}
