﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Pachyderm.Services
{
    public class SettingsService
    {
        private const string _settingsFileName = "usersettings.json";

        private ConfigurationService _configurationService;
        private Dictionary<string, object> _settings;

        public SettingsService(ConfigurationService configurationService)
        {
            _configurationService = configurationService;

            LoadSettings();
        }

        public T GetSetting<T>(string key) where T : new()
        {
            object hit;
            _settings.TryGetValue(key, out hit);

            if (hit is T)
                return (T)hit;
            else
                return default(T);
        }

        public T GetSetting<T>(string key, T defaultVaule) where T : new()
        {
            if (_settings.ContainsKey(key))
                return GetSetting<T>(key);

            _settings.Add(key, defaultVaule);
            return defaultVaule;
        }

        public void SetSetting<T>(string key, T value) where T : new()
        {
            if (_settings.ContainsKey(key))
                _settings[key] = value;
            else
                _settings.Add(key, value);
        }

        public void SaveSettings()
        {
            var filePath = GetSettingsFilePath();

            try
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write((new JavaScriptSerializer()).Serialize(_settings));
                }
            }
            catch { }
        }

        public void LoadSettings()
        {
            var filePath = GetSettingsFilePath();

            if (File.Exists(filePath))
            {
                try
                {
                    using (var reader = new StreamReader(filePath))
                    {
                        _settings = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(reader.ReadToEnd());
                    }
                }
                catch { }
            }
            else
            {
                _settings = new Dictionary<string, object>();
            }
        }

        private string GetSettingsFilePath()
        {
            return Path.Combine(_configurationService.GetApplicationDataFolderPath(), _settingsFileName);
        }
    }
}
